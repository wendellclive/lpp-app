import 'package:flutter/material.dart';

class CadastroPaciente extends StatefulWidget {
  const CadastroPaciente({Key? key}) : super(key: key);

  @override
  _CadastroPacienteState createState() => _CadastroPacienteState();
}

class _CadastroPacienteState extends State<CadastroPaciente> {

  String _escolhaUsuario = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Cadastrar Paciente")
        ),
        body: Container(
          padding: EdgeInsets.all(10),
          child: SingleChildScrollView(
              child:
              Column(
                  children: [
                    Row (
                        children: [
                          Image.asset("images/paciente_add.png",
                              width: 60,
                              height: 60),
                          Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                "Paciente",
                                style: TextStyle(
                                    fontSize: 25
                                ),
                              )
                          ),
                        ]
                    ),
                    Column (
                        children: [
                          TextField(
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                labelText: "Nome"
                            ),
                            style: TextStyle(
                                fontSize: 22
                            ),
                            controller: null,
                          ),
                          TextField(
                            keyboardType: TextInputType.datetime,
                            decoration: InputDecoration(
                                labelText: "Data de Nascimento"
                            ),
                            style: TextStyle(
                                fontSize: 22
                            ),
                            controller: null,
                          ),
                          TextField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: "Altura (m). Ex: (1.81)"
                            ),
                            style: TextStyle(
                                fontSize: 22
                            ),
                            controller: null,
                          ),
                          TextField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: "Peso (kg). Ex: (77.5)"
                            ),
                            style: TextStyle(
                                fontSize: 22
                            ),
                            controller: null,
                          ),
                          Row(
                              children: [
                                Text(
                                  "Masculino",
                                  style: TextStyle(
                                      fontSize: 22
                                  ),
                                ),
                                Radio(
                                    value: "m",
                                    groupValue: _escolhaUsuario,
                                    onChanged: (escolha) {
                                      setState(() {
                                        _escolhaUsuario = escolha.toString();
                                      });
                                      print("resultado: " + escolha.toString());
                                    }),
                                Text(
                                  "Feminino",
                                  style: TextStyle(
                                      fontSize: 22
                                  ),
                                ),
                                Radio(
                                    value: "f",
                                    groupValue: _escolhaUsuario,
                                    onChanged: (escolha) {
                                      setState(() {
                                        _escolhaUsuario = escolha.toString();
                                      });
                                    }),
                              ]
                          ),
                          ElevatedButton(
                            child: Text("Salvar"),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.green,
                            ),
                            onPressed: (){
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CadastroPaciente()
                                  )
                              );
                            },
                          )
                        ]
                    ),
                  ]
              )
          ),
        )
    );
  }
}
