import 'package:flutter/material.dart';

import 'Estagio.dart';
import 'Paciente.dart';
import 'OqueEh.dart';
import 'Orientacao.dart';
import 'Posicao.dart';
import 'Risco.dart';
import 'Video.dart';

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();

}

class _HomeState extends State<Home> {

  double _tamanhoFixo = 75;
  double _tamanhoLogo = 100;
  double _tamanhoSuperior = 10;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.cyan,
          title: Text("LPP PREV - HOME"),
        ),
        body: Container(
            padding: EdgeInsets.only(top: 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("images/logo.png",
                  width: _tamanhoLogo,
                  height: _tamanhoLogo,
                ),
                Padding(
                  padding: EdgeInsets.only(top: _tamanhoSuperior),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                          children: [
                            GestureDetector(
                              onTap: _buscarPaciente,
                              child: Image.asset("images/paciente.png",
                                  width: _tamanhoFixo,
                                  height: _tamanhoFixo),
                            ),
                            Text("Paciente")
                          ]
                      ),
                      Column(
                          children: [
                            GestureDetector(
                              onTap: _oQueEh,
                              child: Image.asset("images/oque.png",
                                  width: _tamanhoFixo,
                                  height: _tamanhoFixo),
                            ),
                            Text("O que é ?")
                          ]
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: _tamanhoSuperior),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                          children: [
                            GestureDetector(
                              onTap: _estagio,
                              child: Image.asset("images/estagio.png",
                                  width: _tamanhoFixo,
                                  height: _tamanhoFixo),
                            ),
                            Text("Estágios")
                          ]),
                      Column(
                          children: [
                            GestureDetector(
                                onTap: _orientacao,
                                child: Image.asset("images/orientacao.png",
                                    width: _tamanhoFixo,
                                    height: _tamanhoFixo)
                            ),
                            Text("Orientações")
                          ]
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: _tamanhoSuperior),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                          children: [
                            GestureDetector(
                              onTap: _risco,
                              child: Image.asset("images/risco.png",
                                  width: _tamanhoFixo,
                                  height: _tamanhoFixo),
                            ),
                            Text("Risco")
                          ]),
                      Column(
                          children: [
                            GestureDetector(
                                onTap: _posicao,
                                child: Image.asset("images/posicao.png",
                                    width: _tamanhoFixo,
                                    height: _tamanhoFixo)
                            ),
                            Text("Posições")
                          ]
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: _tamanhoSuperior),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                          children: [
                            GestureDetector(
                              onTap: _video,
                              child: Image.asset("images/video.png",
                                  width: _tamanhoFixo,
                                  height: _tamanhoFixo),
                            ),  Text("Vídeos")
                          ]
                      )
                    ],
                  ),
                ),
              ],
            )
        )
    );
  }

  void _buscarPaciente() {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Paciente()));
  }

  void _oQueEh() {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => OqueEh()));
  }

  void _estagio() {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Estagio()));
  }

  void _orientacao() {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Orientacao()));
  }

  void _risco() {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Risco()));
  }

  void _posicao() {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Posicao()));
  }

  void _video() {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Video()));
  }
}
