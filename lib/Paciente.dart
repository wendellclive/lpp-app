import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'CadastroPaciente.dart';

class Paciente extends StatefulWidget {
  const Paciente({Key? key}) : super(key: key);

  @override
  _PacienteState createState() => _PacienteState();
}

class _PacienteState extends State<Paciente> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Buscar Paciente")
      ),
      body: Container(
          padding: EdgeInsets.all(10),
          child:
          Column(
              children: [
                Row (
                    children: [
                      Image.asset("images/paciente.png",
                          width: 60,
                          height: 60),
                      Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "Pacientes",
                            style: TextStyle(
                                fontSize: 25
                            ),
                          )
                      ),
                    ]
                ),
                Row (
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: ElevatedButton(
                            child: Text("Inserir Novo Paciente"),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(15),
                              primary: Colors.green,
                            ),
                            onPressed: (){
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CadastroPaciente()
                                  )
                              );
                            },
                          )
                      ),
                    ]
                ),
                Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                          padding: EdgeInsets.only(top: 15),
                          child: Text(
                            "Lista de Pacientes:"
                          )
                      ),
                    ]
                )
              ]
          )
      ),
    );
  }
}
